# Installazione Python 3

sudo apt-get update

sudo apt-get upgrade

sudo apt-get install python3-pip python3-dev curl

git clone https://antoniofalcetta@bitbucket.org/antoniofalcetta/prjext1.git

cd prjext1

source venv/bin/activate

pip3 install -r requirements.txt

python3 manage.py runserver

# Link frontend
http://127.0.0.1:8000

# Admin area
http://127.0.0.1:8000/admin/

Username: admin

Password: admin