# Generated by Django 3.0.7 on 2020-06-11 18:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('items', '0007_auto_20200610_2022'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='item',
            name='qrcode',
        ),
        migrations.AddField(
            model_name='item',
            name='qr_code',
            field=models.ImageField(blank=True, upload_to='qr_codes'),
        ),
    ]
