from django.urls import path

from django.conf.urls import url

from . import views

urlpatterns = [
    path('', views.index, name='items'),
    path('searchWithCam/', views.searchWithCam, name='search_cam'),
    path('detectWithCam/', views.detectWithCam, name='detect_item'),
    path('<int:pk>/', views.item_detail, name='item_detail'),
]