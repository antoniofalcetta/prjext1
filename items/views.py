from django.shortcuts import render, redirect

from django.core.files.storage import FileSystemStorage

# Librerie Computer Vision
import cv2
import numpy as np
from pyzbar.pyzbar import decode
from datetime import datetime

# Model
from .models import Item


def index(request):
    items = Item.objects.all
    return render(request, 'items/index.html', {'items': items})


def item_detail(request, pk):
    item = Item.objects.get(id=pk)
    return render(request, 'items/detail.html', {'item': item})      


# RICERCA PRODOTTO DA WEBCAM
def searchWithCam(request):

    # Bianco e nero
    bg_mode = False

    # Timestamp
    dt_mode = False

    # Apertura webcam predefinita
    cap = cv2.VideoCapture(0)

    if(not cap):
    #if(not cap.read()[0]):
        print("Webcam non disponibile.")
        exit(0)

    cap.set(3,640)
    cap.set(4,480)

    # DB
    # CONVERSIONE IN ARRAY
    itemcode = Item.objects.values_list('itemcode', flat=True)
    myDataList = list(itemcode)

    while(cap.isOpened()):

        # Catturo il frame
        success, img = cap.read()

        # Imposta il frame in bianco e nero
        if(bg_mode):
            # Conversione frame in bianco e nero
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        
        # Imposta il timestamp
        if(dt_mode):
            now = datetime.now()
            str_now = now.strftime("%d/%m/%Y %H:%M:%S")
            cv2.putText(img, str_now, (20, img.shape[0]-20), cv2.FONT_HERSHEY_PLAIN, 1.5, (255,255,255), 2)

        for barcode in decode(img):
            myData = barcode.data.decode('utf-8')
            
            # In caso di Debug
            #print(myData[0])

            if myData in myDataList:
                myOutput = 'OK'
                myColor = (0,255,0)

                # Restituisco l'id del prodotto
                item = Item.objects.get(itemcode=myData)
                # Converto in string
                itmid = str(item.id)

                cap.release()
                cv2.destroyAllWindows()
                return redirect('/items/' + itmid)
            else:
                myOutput = 'Non Trovato'
                myColor = (0, 0, 255)

                # Creazione record
                Item.objects.get_or_create(itemcode=myData, 
                    itemname='Prodotto generato automaticamente',
                    description='Descrivi qui il tuo prodotto.',
                    qr_code='',
                    price=0)
                
                cap.release()
                cv2.destroyAllWindows()
                return redirect('/items')

            pts = np.array([barcode.polygon],np.int32)
            pts = pts.reshape((-1,1,2))
            cv2.polylines(img,[pts],True,myColor,5)
            pts2 = barcode.rect
            cv2.putText(img,myOutput,(pts2[0],pts2[1]),cv2.FONT_HERSHEY_SIMPLEX,
                        0.9,myColor,2)

        cv2.imshow('Result',img)
        # Lascio la webcam attiva
        k = cv2.waitKey(1)

        # Attiva modalità bianco e nero premendo il tasto b
        if(k == ord('b')):
            bg_mode = not bg_mode
        # Attivo la data nel frame premendo il tasto t
        elif(k == ord('t')):
            dt_mode = not dt_mode            
        # Chiudo la webcam premendo il tasto q
        elif(k == ord('q')):
            break        

    # Rilascio la webcam
    cap.release()
    cv2.destroyAllWindows()

    return redirect('/')


# UTILIZZO COUPON
def detectWithCam(request):

    # Bianco e nero
    bg_mode = False

    # Timestamp
    dt_mode = False

    # Apertura webcam predefinita
    cap = cv2.VideoCapture(0)

    if(not cap):
    #if(not cap.read()[0]):
        print("Webcam non disponibile.")
        exit(0)

    cap.set(3,640)
    cap.set(4,480)

    # DB
    # CONVERSIONE IN ARRAY
    itemcode = Item.objects.values_list('itemcode', flat=True)
    myDataList = list(itemcode)

    while(cap.isOpened()):

        # Catturo il frame
        success, img = cap.read()

        # Frame bianco e nero
        if(bg_mode):
            # Conversione frame in bianco e nero
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        
        # Imposta il timestamp
        if(dt_mode):
            now = datetime.now()
            str_now = now.strftime("%d/%m/%Y %H:%M:%S")
            cv2.putText(img, str_now, (20, img.shape[0]-20), cv2.FONT_HERSHEY_PLAIN, 1.5, (255,255,255), 2)

        for barcode in decode(img):
            myData = barcode.data.decode('utf-8')
            
            # In caso di Debug
            #print(myData)

            if myData in myDataList:
                myOutput = 'OK'
                myColor = (0,255,0)
            else:
                myOutput = 'Non Trovato'
                myColor = (0, 0, 255)

                # Creazione record
                Item.objects.get_or_create(itemcode=myData)

            pts = np.array([barcode.polygon],np.int32)
            pts = pts.reshape((-1,1,2))
            cv2.polylines(img,[pts],True,myColor,5)
            pts2 = barcode.rect
            cv2.putText(img,myOutput,(pts2[0],pts2[1]),cv2.FONT_HERSHEY_SIMPLEX,
                        0.9,myColor,2)

        cv2.imshow('Result',img)
        # Lascio la webcam attiva
        k = cv2.waitKey(1)

        # Attiva modalità bianco e nero premendo il tasto b
        if(k == ord('b')):
            bg_mode = not bg_mode
        # Attivo la data nel frame premendo il tasto t
        elif(k == ord('t')):
            dt_mode = not dt_mode            
        # Chiudo la webcam premendo il tasto q
        elif(k == ord('q')):
            break        

    # Rilascio la webcam
    cap.release()
    cv2.destroyAllWindows()

    return redirect('/items')    