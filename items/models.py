from django.db import models

import qrcode
from io import BytesIO
from django.core.files import File
from PIL import Image, ImageDraw

class Item(models.Model):
    itemcode = models.CharField(max_length=255, blank=True)
    itemname = models.CharField(max_length=255, blank=True)
    description = models.TextField(max_length=255, blank=True)
    qr_code = models.ImageField(upload_to='qr_codes', blank=True)
    price = models.DecimalField(max_digits=6, decimal_places=2, blank=True)

    # class Meta:
    #     verbose_name = 'Item'
    #     verbose_name_plural = 'Items'

    def __str__(self):
        return self.itemname

    def save(self, *args, **kwargs):
        qrcode_img = qrcode.make(self.itemcode)
        canvas = Image.new('RGB', (290, 290), 'white')
        draw = ImageDraw.Draw(canvas)
        canvas.paste(qrcode_img)
        #fname = f'qr_code-{self.itemname}'+'.png'
        # NOME COUPON
        fname = f'qr_code-{self.itemname}.png'
        buffer = BytesIO()
        canvas.save(buffer,'PNG')
        self.qr_code.save(fname, File(buffer), save=False)
        #canvas.close()
        super().save(*args, **kwargs)
