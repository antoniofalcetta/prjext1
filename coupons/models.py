from django.db import models

import qrcode
from io import BytesIO
from django.core.files import File
from PIL import Image, ImageDraw

class Coupon(models.Model):
    customer = models.CharField(max_length=255, blank=True)
    coupon_code = models.CharField(max_length=255, blank=True)
    coupon_attachment = models.ImageField(upload_to='qr_coupons', default='default.png', blank=True)
    state_coupon = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return self.customer

    def save(self, *args, **kwargs):
        qrcode_img = qrcode.make(self.coupon_code)
        canvas = Image.new('RGB', (290, 290), 'white')
        draw = ImageDraw.Draw(canvas)
        canvas.paste(qrcode_img)
        #fname = f'qr_code-{self.itemname}'+'.png'
        # NOME COUPON
        fname = f'coupon_attachment-{self.customer}.png'
        buffer = BytesIO()
        canvas.save(buffer,'PNG')
        # Nome allegato models.ImageFields()
        self.coupon_attachment.save(fname, File(buffer), save=False)
        #canvas.close()
        super().save(*args, **kwargs)
