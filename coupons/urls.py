from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='coupons'),
    path('index', views.index, name='coupons')
]