from django.shortcuts import render
# Email
from django.core.mail import send_mail
# Settings
from django.conf import settings

# Generazione codice coupon
import random

from coupons.models import Coupon

def index(request):

    if request.method == 'POST':

        customer = request.POST['email']
        coupon_code = get_promo_code(6)
        coupon_attachment = 'default.png'
        state_coupon = 'Generato'


        Coupon.objects.create(customer=customer, coupon_code=coupon_code, coupon_attachment=coupon_attachment, state_coupon=state_coupon)
        # Informazioni da form
        message = 'Richiesta info prodotto: ' + '"' + request.POST['itemname'] + '"' + ' codice coupon: ' + coupon_code + ' (vedi allegato)'

        send_mail('Codice Sconto - Azienda da definire.', message, 
            # Mittente
            settings.EMAIL_HOST_USER,
            # Destinatario
            [request.POST['email']],
            fail_silently=False
        )

    return render(request, 'pages/index.html')

# GENERAZIONE CODICE COUPON
def get_promo_code(num_chars):
    code_chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    code = ''
    for i in range(0, num_chars):
        slice_start = random.randint(0, len(code_chars) - 1)
        code += code_chars[slice_start: slice_start + 1]
    return code